#!/usr/bin/env python
import cv2
import numpy as np
from PIL import ImageFont, ImageDraw, Image

DISPLAY = 'img'

TIEMPO = [
	['Parcialmente soleado', 'parcial.png'],
	['Soleado', 'soleado.png'],
	['Tormenta', 'tormenta.png'],
	['Lluvia', 'lluvia.png']
]

cv2.namedWindow( DISPLAY, cv2.WND_PROP_FULLSCREEN )
cv2.setWindowProperty( DISPLAY, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN )

def colocar_texto( img, texto, pos, tam = 15, col = ( 255, 255, 255, 255 ) ) :
	tam = int( tam )
	font = ImageFont.truetype( './fonts/font.ttf', tam )
	img_pil = Image.fromarray( img )
	draw = ImageDraw.Draw( img_pil )
	draw.text( pos, texto, font = font, fill = col )
	return np.array( img_pil )

def mostrar_clima ( duracion, estado, temperatura ) :
	img = cv2.imread( './imagenes/plantilla.png' )

	ANCHO = img.shape[1]
	ALTO = img.shape[0]

	img = colocar_texto( img, f'{temperatura}º C', ( ANCHO * .3, ANCHO * .02 ), ALTO * .2 )
	img = colocar_texto( img, TIEMPO[estado][0], ( ANCHO * .3, ANCHO * .02 + ALTO * .2 ), ALTO * .1 )

	img2 = cv2.imread( f'./imagenes/{TIEMPO[estado][1]}' )

	offx = int( ANCHO * .05 )
	offy = int( ANCHO * .02 )

	for x in range( img2.shape[1] ) :
		for y in range( img2.shape[0] ) :
			img[x + offy][y + offx] = img2[x][y]

	cv2.imshow( DISPLAY, img )
	cv2.waitKey( duracion * 1000 )

mostrar_clima( 10, 0, 18 )

cv2.destroyAllWindows()