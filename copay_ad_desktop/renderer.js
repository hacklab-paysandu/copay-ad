const ipcRenderer = require( 'electron' ).ipcRenderer
const spawn = require( 'child_process' ).spawn
const fs = require( 'fs' )
const request = require( 'request' )

var pathNuevo = require('path'), filePath = pathNuevo.join(__dirname, 'host.conf');
let ipServer = '';

fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    if (!err) {
        console.log(data);
        ipServer = data;
    } else {
        console.log(err);
    }
});


imgs = ['jpg', 'png']
vids = ['mp4']

 
let hayCambios = false
let path = ''
let cod = 0
let codNot = 0
let info = []
let infoNot = []
let cantNoticias = 0

let isWin = process.platform === 'win32'

eliminar = () => {
	cod = 0
	info = []
	files.innerHTML = ''
	hayCambios = false
	mensaje.innerText = ''
	path = ''
	cambios( false )
	$( '#btnGuardarComo' ).attr( 'disabled', true )
}

$( '#btnSubir' ).click( () => {
	ipcRenderer.send( 'subir-archivo' )
} )

$( '#btnNuevo' ).click( () => {


	

	if ( info.length !== 0 && hayCambios )
		ipcRenderer.send( 'new-borrar' )
	else
		eliminar()
} )

abrirArchivo = () => {
	ipcRenderer.send( 'abrir-archivo' )
}

$( '#btnAbrir' ).click( () => {
	if ( hayCambios )
		ipcRenderer.send( 'descartar-actual' )
	else
		abrirArchivo()
} )


//este es codigo mio
$( '#btnNoticia' ).click( () => {
	ipcRenderer.send( 'pag-noticia' )
})

$( '#btnRegresar' ).click( () => {
	ipcRenderer.send( 'pag-volver' )
})


$( '#btnSubirNot' ).click( () => {
	ipcRenderer.send( 'subir-archivoNot' )
} )


ipcRenderer.on( 'subir-archivoNot', ( e, args ) => {
	if ( args === null )
		return

	request( {
		method: 'POST',
		uri: 'http://'+ipServer+':8080/uploadNot',
		headers: { 'Content-Type': 'multipart/form-data' },
		multipart: [ { body: fs.createReadStream( args[0] ) } ]
	}, ( e, res, body ) => {
		if ( e )
			return ipcRenderer.send( 'mensaje', 'No se pudo subir el archivo' )

		ipcRenderer.send( 'mensaje', 'Archivo subido' )
	} )
} )




$( '#btnAgregarNot' ).click( () => {
//tuve que sacar esto porque solamente necesitan descripcion
//titu = document.getElementById('tituNot').value
titu = "no"
desc = document.getElementById('desc').value

if (titu =="" || desc =="") {

	ipcRenderer.send( 'mensaje', 'Faltan campos que completar' )
} else{



tieneImg = document.getElementById('lblEscondida').innerHTML
if (tieneImg=='si') {
	imagencitaPath = document.getElementById('imagencitaNot').innerHTML
var nombre = imagencitaPath.split( isWin ? '\\' : '/' )
		nombre = nombre[nombre.length - 1]
}else{
var nombre = 'no'
imagencitaPath = 'no'	
}


	infoNot.push( {
			id: codNot,
			titulo: titu,
			descripcion: desc,
			duracion: 10,
			imgPath: imagencitaPath,
			nombreImg: nombre

		} )
		codNot++

ipcRenderer.send( 'mensaje', 'Noticia agregada!' )

document.getElementById('tituNot').value = ""
document.getElementById('desc').value = ""
document.getElementById('imagencitaNot').innerHTML="¿Querés agregar alguna imagen? (es opcional)"
document.getElementById('desc').setAttribute('maxlength',100)
document.getElementById('maximum').textContent='/ 100'
document.getElementById('current').textContent='0'
document.getElementById('lblEscondida').innerHTML = 'no'
cambios( true )
cantNoticias++
if(cantNoticias == 1){

document.getElementById('cantNot').innerHTML = cantNoticias +" noticia agregada"
}else{
document.getElementById('cantNot').innerHTML = cantNoticias +" noticias agregadas"
}
}
})


$( '#btnGuardarNot' ).click( () => {
	if(cantNoticias == 0){
		ipcRenderer.send('mensaje', 'No hay noticias para guardar')
	}else{
	ipcRenderer.send( 'dir-guardadoNot' )
}
})




guardarNoticia = ( p ) => {
	let content = ''
	let desc = ''
	let suma = 0
	infoNot.map( i => {
		content += i.titulo + '&' + i.duracion + '&' + i.imgPath + '&' + i.nombreImg + '&' + suma + '\n'
		desc += i.descripcion + '&' + suma + '&' + '-*-'
		suma++
	} )

	fs.writeFile( 'tempNot.ca', content, ( err ) => {
		if ( err )
			return ipcRenderer.send( 'mensaje', 'Error al guardar: ' + err )


			fs.writeFile("descripcion.txt", desc, (err) => {
 		 if (err) console.log(err);
  			console.log("Successfully Written to File.");
			});

		let process = spawn( 'python', ['./compilarNot.py', p] )
		process.stdout.on( 'data', ( data ) => { console.log( data.toString() ) } )
		process.on( 'close', ( code ) => {

		path = p
			mensaje.innerText = ""
			cambios( false )
			ipcRenderer.send( 'mensaje', 'Guardado!' )
		} )
	} )
}


ipcRenderer.on( 'guardar-enNot', ( e, args ) => {
	if ( args === null )
		return

	guardarNoticia( args )
} )


$( '#btnImagencitaNot' ).click( () => {
	ipcRenderer.send( 'select-Imagen' )
} )

ipcRenderer.on( 'agregadoImg', ( e, args ) => {
	if ( args !== null )
	{
	document.getElementById('imagencitaNot').innerHTML = args[0]
	document.getElementById('imagenMuestra').src = args[0]
	document.getElementById('lblEscondida').innerHTML = 'si'
	document.getElementById('desc').setAttribute('maxlength',60)
	document.getElementById('maximum').textContent='/ 60'
	}
} )


//hasta aca



$( '#btnAgregar' ).click( () => {
	ipcRenderer.send( 'select-file' )
} )

guardar_cambios = () => {
	if ( path !== '' )
		guardar( path )
	else
		ipcRenderer.send( 'dir-guardado' )
}

$( '#btnGuardar' ).click( () => {
	if ( info.length === 0 )
		return ipcRenderer.send( 'mensaje', 'No hay archivos para guardar' )

	guardar_cambios()
} )

$( '#btnGuardarComo' ).click( () => {
	if ( info.length === 0 )
		return ipcRenderer.send( 'mensaje', 'No hay archivos para guardar' )

	ipcRenderer.send( 'dir-guardado' )
} )

cambios = ( c ) => {
	hayCambios = c
	$( '#btnGuardar' ).attr( 'disabled', !c )

	if ( c )
		mensaje.innerHTML = ( path === '' ? 'Guarde el archivo' : path ) + ' <strong>*</strong>'
}

showFiles = () => {
	files.innerHTML = ''


/*
un info.map(i => ...) es lo mismo que hacer
 for(int i=0; i<info.length;i++){
	info[i] ...
}
*/
	info.map( i => {
		let file = document.getElementById( 'tmp-file' ).content.cloneNode( true )
		file.querySelector( '.archivo' ).innerText = i.nombre
		file.querySelector( 'button' ).onclick = () => { 
			ipcRenderer.send( 'delete-file', { nombre: i.nombre, id: i.id } ) 
			cambios(true)
		}
			
//este codigo lo modifique yo
			if(i.type==='i'){
				let numerito = file.querySelector('input')
				numerito.setAttribute("type","text")
				numerito.setAttribute("value",i.duracion)

				numerito.onchange = function( e ) {
					i.duracion = numerito.value
					cambios(true)
				}
			}
			//hasta aca modifique yo
				files.append( file )
	} )
}

guardar = ( p ) => {
	let content = ''

	info.map( i => {
		content += i.path + ',' + i.nombre + ',' + i.type + ',' + i.duracion + '\n'
	} )

	fs.writeFile( 'temp.ca', content, ( err ) => {
		if ( err )
			return ipcRenderer.send( 'mensaje', 'Error al guardar: ' + err )

		let process = spawn( 'python', ['./compilar.py', p] )
		process.stdout.on( 'data', ( data ) => { console.log( data.toString() ) } )
		process.on( 'close', ( code ) => {
			path = p
			mensaje.innerText = path
			cambios( false )
			ipcRenderer.send( 'mensaje', 'Guardado!' )
		} )
	} )
}

ipcRenderer.on( 'agregado', ( e, args ) => {
	if ( args !== null )
	{
		var nombre = args[0].split( isWin ? '\\' : '/' )
		nombre = nombre[nombre.length - 1]

//esto anda por si esta repetido lo que se quiere ingresar
		let repetido = false
		info.forEach( ( v, i, a ) => {
			if ( v.nombre === nombre ){
				console.log('esta repetido')
				repetido = true

			}
		})
//hasta aca
	 if ( !repetido) {

		let vid = false
		vids.map( v => { if ( nombre.includes( v ) ) vid = true } )

		info.push( {
			id: cod,
			path: args[0],
			nombre: nombre,
			type: vid ? 'v' : 'i',
			//modifique yo
			duracion: 10

		} )
		cod++

		showFiles()
		cambios( true )
	 }
	}
} )

ipcRenderer.on( 'borrar-archivo', ( e, args ) => {
	info.forEach( ( v, i, a ) => {
		if ( v.id === args ){

	//aca empieza codigo mio
		/*	console.log(path)
			console.log({v,i,a})*/

			if(path!==''){
				let process = spawn( 'python', ['./borrarArchivo.py', path , v.nombre ] )
				process.stdout.on( 'data', ( data ) => { console.log( data.toString() ) } )

	/*	process.on( 'close', ( code ) => {
			path = p
			mensaje.innerText = path
			cambios( false )
			ipcRenderer.send( 'mensaje', 'Guardado!' )
		} )*/
			}

			//aca termina codigo mio
			//en la posicion "i" borrar 1 elemento
			a.splice( i, 1 )
			
			
		}
	} )

	showFiles()
} )

ipcRenderer.on( 'nuevo', ( e, args ) => {
	if ( args === 1 )
		eliminar()
	else if ( args === 2 )
		guardar_cambios()
} )

ipcRenderer.on( 'guardar-en', ( e, args ) => {
	if ( args === null )
		return

	guardar( args )
} )

ipcRenderer.on( 'descartar', ( e, args ) => {
	eliminar()
	abrirArchivo()
} )

ipcRenderer.on( 'abrir-archivo', ( e, args ) => {
	if ( args === null )
		return

	path = args[0]
	mensaje.innerText = path

	let process = spawn( 'python', ['./abrir.py', __dirname, path] )
	process.stdout.on( 'data', ( data ) => { console.log( data.toString() ) } )
	process.on( 'close', ( code ) => {
		fs.readFile( './contenido/info.ca', ( err, contenido ) => {
			if ( err )
				return ipcRenderer.send( 'mensaje', 'Error leyendo el archivo' )

			cod = 0
			contenido.toString().split( '\n' ).map( l => {
				if ( l === '' )
					return

				partes = l.split( ',' )

				info.push( {
					id: cod,
					path: './contenido/' + partes[1],
					nombre: partes[1],
					type: partes[2],
					//duracion modifique yo
					duracion: partes[3]

				} )
				cod++
			} )

			showFiles()
			cambios( false )
		} )
	} )
} )

ipcRenderer.on( 'subir-archivo', ( e, args ) => {
	if ( args === null )
		return

	request( {
		method: 'POST',
		uri: 'http://'+ipServer+':8080/upload',
		headers: { 'Content-Type': 'multipart/form-data' },
		multipart: [ { body: fs.createReadStream( args[0] ) } ]
	}, ( e, res, body ) => {
		if ( e )
			return ipcRenderer.send( 'mensaje', 'No se pudo subir el archivo' )

		ipcRenderer.send( 'mensaje', 'Archivo subido' )
	} )
} )

showFiles()
cambios( false )
$( '#btnGuardarComo' ).attr( 'disabled', true )