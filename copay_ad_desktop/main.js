const { app, BrowserWindow, ipcMain, dialog } = require( 'electron' )

createWindow = () =>
{
	let win = new BrowserWindow( { width: 800, height: 600 } )
	win.setMenu( null )
	win.loadFile( 'index.html' )

	win.webContents.on( 'before-input-event', ( event, input ) => {
		if ( input.type === 'keyUp' )
			if ( input.key === 'F12' )
					win.webContents.toggleDevTools();
	} )

	ipcMain.on( 'select-file', ( e, args ) => {
		let file = dialog.showOpenDialog( win, {
			properties: ['openFile'],
			filters: [
				{ name: 'File', extensions: ['jpg', 'png', 'mp4'] }
			]
		} )

		e.sender.send( 'agregado', file )
	} )




//este es codigo mio
	ipcMain.on( 'pag-noticia', (e, args) => {
		win.loadFile('nuevaNot.html')
	})

	ipcMain.on( 'pag-volver', (e, args) => {
		win.loadFile('index.html')
	})


	ipcMain.on( 'dir-guardadoNot', ( e, args ) => {
		dialog.showSaveDialog( win, {
			defaultPath: '/noticias',
			filters: [ { name: 'COPAY ad', extensions: ['ca'] } ]
		}, ( path ) => {
			e.sender.send( 'guardar-enNot', path )
		} )
	} )

	ipcMain.on( 'subir-archivoNot', ( e, args ) => {
		dialog.showOpenDialog( win, {
			properties: ['openFile'],
			filters: [{ name: 'COPAY File', extensions: ['ca'] }]
		}, ( res ) => {
			e.sender.send( 'subir-archivoNot', res )
		} )
	} )

	ipcMain.on( 'select-Imagen', ( e, args ) => {
		let file = dialog.showOpenDialog( win, {
			properties: ['openFile'],
			filters: [
				{ name: 'File', extensions: ['jpg', 'png'] }
			]
		} )

		e.sender.send( 'agregadoImg', file )
	} )


//hasta aca


	ipcMain.on( 'delete-file', ( e, args ) => {
		dialog.showMessageBox( win, {
			type: 'question',
			buttons: ['No', 'Si'],
			defaultId: 0,
			message: '¿Quitar archivo ' + args.nombre + '?'
		}, ( res ) => {
			if ( res )
				e.sender.send( 'borrar-archivo', args.id )
		} )
	} )
 //ipcMain.on significa que el ipcMain se queda escuchando a que le llegue un msg "new-borrar",junto a unos args y cuando le llegue, hace lo que esta aca dentro
	

	ipcMain.on( 'new-borrar', ( e, args ) => {
		dialog.showMessageBox( win, {
			type: 'question',
			buttons: ['Cancelar', 'Descartar actual', 'Guardar cambios'],
			defaultId: 0,
			message: '¿Abrir nuevo?',
			detail: 'Si no guarda la lista actual se descartarán los cambios'
		}, ( res ) => {

			//con el sender.send haces el llamado al ipc, junto con los args, para que lo que esta escuchando se ejecute
			e.sender.send( 'nuevo', res )
		} )
	} )

	ipcMain.on( 'dir-guardado', ( e, args ) => {
		dialog.showSaveDialog( win, {
			defaultPath: '/home/jorge/Desktop',
			filters: [ { name: 'COPAY ad', extensions: ['ca'] } ]
		}, ( path ) => {
			e.sender.send( 'guardar-en', path )
		} )
	} )

	ipcMain.on( 'descartar-actual', ( e, args ) => {
		dialog.showMessageBox( win, {
			type: 'question',
			buttons: ['Cancelar', 'Descartar'],
			defaultId: 0,
			message: '¿Los cambios no guardados serán descartados?'
		}, ( res ) => {
			if ( res === 1 )
				e.sender.send( 'descartar' )
		} )
	} )

	ipcMain.on( 'abrir-archivo', ( e, args ) => {
		dialog.showOpenDialog( win, {
			properties: ['openFile'],
			filters: [{ name: 'COPAY File', extensions: ['ca'] }]
		}, ( res ) => {
			e.sender.send( 'abrir-archivo', res )
		} )
	} )

	ipcMain.on( 'subir-archivo', ( e, args ) => {
		dialog.showOpenDialog( win, {
			properties: ['openFile'],
			filters: [{ name: 'COPAY File', extensions: ['ca'] }]
		}, ( res ) => {
			e.sender.send( 'subir-archivo', res )
		} )
	} )

	ipcMain.on( 'mensaje', ( e, mensaje ) => {
		dialog.showMessageBox( win, {
			message: mensaje,
			buttons: ['Ok']
		} )
	} )

	win.on( 'closed', () => {
		win = null
	} )
}

app.on( 'ready', createWindow )

app.on( 'window-all-closed', () => {
	if ( process.platform !== 'darwin' )
		app.quit()
} )

app.on( 'activate', () => {
	if ( win === null )
		createWindow()
} )