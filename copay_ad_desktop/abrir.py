#!/usr/bin/env python3
import sys
import os
import platform

DIR = sys.argv[1]
FILE = sys.argv[2]
"""print(DIR)
print(FILE)"""

if platform.system() is 'Windows' :
	os.system( 'RMDIR /S /Q contenido' )
	os.system( 'DEL *.ca' ) # test

	os.system( 'COPY "{}" "{}"'.format( FILE, os.path.join( DIR, 'copay.ca' ) ) )
	os.system( 'unzip.exe -o copay.ca' )
else :
	os.system( 'rm -r contenido' )
	os.system( 'rm -r *.ca' )

	os.system( 'cp "{}" "{}"'.format( FILE, os.path.join( DIR, 'copay.ca' ) ) )
	os.system( 'unzip -o copay.ca' )