#!/usr/bin/env python3
import os
import sys
import platform

IS_WIN = platform.system() is 'Windows'

path = sys.argv[1] if len( sys.argv ) > 1 else 'COPAY.ca'
print(path)
if IS_WIN :
   # os.system( 'RMDIR /S /Q .contenido' )
    print('borrando contenido')
    os.system( 'MKDIR contenido' )
else :
   # os.system( 'rm -r .contenido' )
    os.system( 'mkdir contenido' )

print( 'Leyendo archivos' )
f = open( 'temp.ca', 'r' )

for l in f :
    print("y aca si entra")
    l = l.replace( '\n', '' )
    p = l.split( ',' )
    file = p[0]
    
    print( 'Copiando {}'.format( file ) )
    if IS_WIN :
    	os.system( 'COPY "{}" contenido'.format( file ) )
    else :
    	os.system( 'cp "{}" contenido'.format( file ) )

f.close()

print( 'Comprimiendo' )
if IS_WIN :
    os.system( 'DEL {}'.format( path ) )
    os.system( 'MOVE temp.ca contenido\\info.ca' )
    os.system( 'zip.exe -r "{}" contenido'.format( path ) )
    os.system( 'RMDIR /S /Q contenido' )
else :
    os.system( 'rm {}'.format( path ) )
    os.system( 'mv temp.ca contenido/info.ca' )
    os.system( 'zip -r "{}" contenido'.format( path ) )
    os.system( 'rm -r contenido' )
