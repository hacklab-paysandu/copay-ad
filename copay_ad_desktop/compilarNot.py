#!/usr/bin/env python3
import os
import sys
import platform

IS_WIN = platform.system() is 'Windows'

path = sys.argv[1] if len( sys.argv ) > 1 else 'COPAY.ca'

if IS_WIN :
   # os.system( 'RMDIR /S /Q .contenido' )
    print('borrando contenido')
    os.system( 'MKDIR .contenidoNot' )
else :
   # os.system( 'rm -r .contenido' )
    os.system( 'mkdir .contenidoNot' )


f = open( 'tempNot.ca', 'r' )

for l in f :
    #l = l.replace( '\n', '' )
    p = l.split( '&' )
    file = p[2]
    print(file)
    
    print( 'Copiando {}'.format( file ) )
    if IS_WIN :
        os.system( 'COPY "{}" .contenidoNot'.format( file ) )
    else :
        os.system( 'cp "{}" .contenidoNot'.format( file ) )

f.close()





print( 'Comprimiendo' )
if IS_WIN :
    os.system( 'DEL {}'.format( path ) )
    os.system( 'MOVE tempNot.ca .contenidoNot\\infoNot.ca' )
    os.system( 'MOVE descripcion.txt .contenidoNot\\descripcion.txt' )
    os.system( 'zip.exe -r "{}" .contenidoNot'.format( path ) )
    os.system( 'RMDIR /S /Q .contenidoNot' )
else :
    os.system( 'rm {}'.format( path ) )
    os.system( 'mv tempNot.ca .contenidoNot/infoNot.ca' )
    os.system( 'zip -r "{}" .contenidoNot'.format( path ) )
    os.system( 'rm -r .contenidoNot' )
