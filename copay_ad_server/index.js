const fs = require( 'fs' )
const http = require( 'http' )
const app = require( 'express' )()
const formidable = require( 'formidable' )
const mv = require( 'mv' )

const puertoHTTP = 8080
const puertoFTP = 8000

http.createServer( ( req, res ) => {
	res.writeHead( 200, { 'Content-Type': 'text/plain' } )

	if ( req.url === '/check' )
	{
		fs.readFile( 'check.txt', ( err, data ) => {
			if ( err )
			{
				fs.writeFileSync( 'check.txt', 0 )
				res.writeHead( 200, { 'Content-Type': 'text/plain' } )
				res.write( '0' )
				res.end()
			}
			else
			{
				res.writeHead( 200, { 'Content-Type': 'text/plain' } )
				res.write( ( +data ) + '' )
				res.end()
			}
		} )
	}
	else if ( req.url === '/upload' )
	{
		new formidable.IncomingForm().parse( req, ( e, fields, files ) => {
			let oldpath = files.null.path
			let newpath = __dirname + '/copay.ca'

			mv( oldpath, newpath, ( e ) => {
				if ( e )
				{
					res.write( 'Err: ' + e )
					res.end()
					return
				}

				fs.readFile( 'check.txt', ( err, data ) => {
					if ( err )
						fs.writeFileSync( 'check.txt', 0 )
					else
						fs.writeFileSync( 'check.txt', ( +data + 1 ) )

					res.write( 'ok' )
					res.end()
				} )
			} )
		} )
	}
	else if ( req.url === '/uploadNot' )
	{
		new formidable.IncomingForm().parse( req, ( e, fields, files ) => {
			let oldpath = files.null.path
			let newpath = __dirname + '/noticias.ca'

			mv( oldpath, newpath, ( e ) => {
				if ( e )
				{
					res.write( 'Err: ' + e )
					res.end()
					return
				}

				fs.readFile( 'checkNot.txt', ( err, data ) => {
					if ( err )
						fs.writeFileSync( 'checkNot.txt', 0 )
					else
						fs.writeFileSync( 'checkNot.txt', ( +data + 1 ) )

					res.write( 'ok' )
					res.end()
				} )
			} )
		} )
	} 
	else if ( req.url === '/checkNot' ) 
	{
		fs.readFile( 'checkNot.txt', ( err, data ) => {
			if ( err )
			{
				fs.writeFileSync( 'checkNot.txt', 0 )
				res.writeHead( 200, { 'Content-Type': 'text/plain' } )
				res.write( '0' )
				res.end()
			}
			else
			{
				res.writeHead( 200, { 'Content-Type': 'text/plain' } )
				res.write( ( +data ) + '' )
				res.end()
			}
		} )
	}
	else if( req.url === '/probandoId')
	{
		console.log(req.body);

		res.end();
	}	
	else
	{
		res.write( 'Err' );
		res.end();
	}
} ).listen( puertoHTTP, () => {
	console.log( 'Servidor HTTP iniciado en puerto ' + puertoHTTP )
} )

app.get( '/download', ( req, res ) => {
	
	res.download( './copay.ca', 'copay.ca' )
	
} )

app.get ( '/probandoId', ( req, res ) => {
	console.log(req.query);
})


app.get( '/downloadNot', ( req, res ) => {
	res.download( './noticias.ca', 'noticias.ca' )
	
} )


app.listen( puertoFTP, () => {
	console.log( 'Servidor FTP iniciado en puerto ' + puertoFTP )
} )