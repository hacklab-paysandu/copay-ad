#!/usr/bin/env python
import cv2
import numpy as np
from PIL import ImageFont, ImageDraw, Image, ImageOps
import requests
from xml.dom import minidom
import datetime
import locale
import scriptVideo
import time



def text_wrap(text, font, max_width):
    lines = []
 
    if font.getsize(text)[0] <= max_width:
        lines.append(text) 
    else:
      
        words = text.split(' ')  
        i = 0
      
        while i < len(words):
            line = ''         
            while i < len(words) and font.getsize(line + words[i])[0] <= max_width:                
                line = line + words[i] + " "
                i += 1
            if not line:
                line = words[i]
                i += 1
            
            lines.append(line)    
    return lines



def colocar_texto( img, texto, pos, tam = 20, col = ( 0, 0, 0, 255 ) ) :
	tam = int( tam )
	font = ImageFont.truetype( './fonts/font.ttf', tam )
	img_pil = Image.fromarray( img )
	image_size = img_pil.size
	draw = ImageDraw.Draw( img_pil )
	x = 20
	lineas = text_wrap(texto,font,image_size[0] - x)
	line_height = font.getsize('hg')[1]
	
	y = 10
	for line in lineas:
    		draw.text((x, y), line, font = font , fill = col )
    		y = y + line_height

	return np.array( img_pil )


def colocar_textoSinImg( img, texto, pos, tam = 20, col = ( 0, 0, 0, 255 ) ) :
	tam = int( tam )
	font = ImageFont.truetype( './fonts/font.ttf', tam )
	img_pil = Image.fromarray( img )
	image_size = img_pil.size
	draw = ImageDraw.Draw( img_pil )
	x = 20
	lineas = text_wrap(texto,font,image_size[0] - x)
	line_height = font.getsize('hg')[1]
	
	y = 40
	for line in lineas:
    		draw.text((x, y), line, font = font , fill = col )
    		y = y + line_height

	return np.array( img_pil )


def colocar_textoDesc(img, texto, pos, tam = 15, col = ( 0, 0, 0, 255 ) ) :
	tam = int( tam )
	font = ImageFont.truetype( './fonts/fontDesc.ttf',tam )

	img_pil = Image.fromarray( img )
	image_size = img_pil.size

	draw = ImageDraw.Draw( img_pil )

	lineas = text_wrap(texto,font,image_size[0])
	line_height = font.getsize('hg')[1]
	x = 20
	y = 400
	for line in lineas:
    		draw.text((x, y), line, font = font , fill = col )
    		y = y + line_height

	return np.array( img_pil )


def mostrarImagen(imagen, plantilla):
	ANCHO = plantilla.shape[1]
	ALTO = plantilla.shape[0]
	tamanito_deseado = 1300
	tamanio_viejo = imagen.shape[:2] 

	ratio = float(tamanito_deseado)/max(tamanio_viejo)
	tamanio_nuevo = tuple([int(x*ratio) for x in tamanio_viejo])
	imagen = cv2.resize(imagen, (tamanio_nuevo[1], tamanio_nuevo[0]))


	offx = int( ANCHO * .15 )
	offy = int( ANCHO * .16 )


	color = [0, 0, 0] # 'cause purple!

	top, bottom, left, right = [3]*4

	img_with_border = cv2.copyMakeBorder(imagen, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
	
	"""for x in range( imagen.shape[1] ) :
		for y in range( imagen.shape[0] ) :
			plantilla[y + offy][x + offx] = imagen[y][x]"""

	for x in range( img_with_border.shape[1] ) :
		for y in range( img_with_border.shape[0] ) :
			plantilla[y + offy][x + offx] = img_with_border[y][x]

	return plantilla




def mostrar_noticia (player, duracion, titulo, descripcion, imagenot) :
	img = cv2.imread( './imagenes/plantillaNoticia.png' )

	ANCHO = img.shape[1]
	ALTO = img.shape[0]

	if(imagenot!='no'):
		imageNoticia  = cv2.imread(imagenot)
		imgUlt = mostrarImagen(imageNoticia,img)
		imgUlt = colocar_texto( img, u'{}'.format(descripcion), ( ANCHO * .03, ANCHO * .02 ), ALTO * .11 )
	else :
		imgUlt = colocar_textoSinImg( img, u'{}'.format(descripcion), ( ANCHO * .03, ANCHO * .02 ), ALTO * .15 )
		

	
	cv2.imwrite('./imagenes/noticia.jpg',imgUlt)
	player.showImage( './imagenes/noticia.jpg')
	time.sleep(10)



def mostrar(player, titulo,desc, imagenot):
	mostrar_noticia(player, 20,titulo,desc,imagenot)


