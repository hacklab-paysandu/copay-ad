import os
import sys
import pygame
import time
import subprocess
import random
import glob
import platform

IS_WIN = platform.system() is 'Windows'
# MediaPlayer class - initializes frame buffer and provides methods to showphotos and videos
class mediaplayer:
   
    screen = None;
    screen_height = 0;
    screen_width = 0;
    
    def __init__(self):
        # Assume we're using fbcon on Raspberry Pi
        #os.putenv('SDL_VIDEODRIVER', 'fbcon')

        try:
            pygame.display.init()
        except:
            print ("Unable to initialize frame buffer!")
            sys.exit()

        self.screen_height = pygame.display.Info().current_h
        self.screen_width = pygame.display.Info().current_w
        
        self.screen = pygame.display.set_mode([self.screen_width, self.screen_height], pygame.FULLSCREEN)
        self.screen.fill([0, 0, 0])        

        # This hides the mouse pointer which is unwanted in this application
        pygame.mouse.set_visible(False)
        pygame.display.update()

    def __del__(self):
        pass

    def is_init(self):
    	return pygame.display.get_init()

    def teclaPresionada(self):
    	events = pygame.event.get()
    	for event in events:
    		if event.type == pygame.KEYDOWN:
        		if event.key == pygame.K_ESCAPE:
        			pygame.quit()

        		



    def showImage(self, filename):
        img = pygame.image.load(filename)

        img_height = img.get_height()
        img_width = img.get_width()

        # If the image isn't already the same size as the screen, it needs to be scaled
        if ((img_height != self.screen_height) or (img_width != self.screen_width)):
            # Determine what the height will be if we expand the image to fill the whole width
            scaled_height = int((float(self.screen_width) / img_width) * img_height)

            # If the scaled image is going to be taller than the screen, then limit the maximum height and scale the width instead
            if (scaled_height > self.screen_height):
                scaled_height = self.screen_height
                scaled_width = int((float(self.screen_height) / img_height) * img_width)
            else:
                scaled_width = self.screen_width

            img_bitsize = img.get_bitsize()

            # transform.smoothscale() can only be used for 24-bit and 32-bit images. If this is not a 24-bit or 32-bit
            # image, use transform.scale() instead which will be ugly but at least will work
            if (img_bitsize == 24 or img_bitsize == 32):
                img = pygame.transform.smoothscale(img, [scaled_width, scaled_height])
            else:
                img = pygame.transform.scale(img, [scaled_width, scaled_height])

            # Determine where to place the image so it will appear centered on the screen
            display_x = (self.screen_width - scaled_width) / 2
            display_y = (self.screen_height - scaled_height) / 2
        else:
            # No scaling was applied, so image is already full-screen
            display_x = 0
            display_y = 0

        # Blank screen before showing photo in case it doesn't fill the whole screen
        self.screen.fill([0, 0, 0])
        self.screen.blit(img, [display_x, display_y])
        pygame.display.update()

    def showVideo(self, filename):
        # Videos will not be scaled - this needs to be done during transcoding
        # Blank screen before showing video in case it doesn't fill the whole screen
        
        if IS_WIN :     
            wmp = r"C:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe"
            media_file = os.path.abspath(os.path.realpath(filename))
            subprocess.call([wmp, media_file])

        else :
            self.screen.fill([0, 0, 0])
            pygame.display.update()
            subprocess.call(['/usr/bin/omxplayer', '-o', 'hdmi', filename], shell=False)
            # This might not be necessary, but it's there in case any stray copies of omxplayer.bin are somehow left running
            subprocess.call(['/usr/bin/killall', 'omxplayer.bin'], shell=False)
            #pygame.display.update()
            

