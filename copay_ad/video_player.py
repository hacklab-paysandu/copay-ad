#!/usr/bin/env python3
import os
import platform
import muestraClima
import muestraClimaImgOpen
import noticias
import scriptVideo
import time
import io
import sys
try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

IS_WIN = platform.system() is 'Windows'
HOST = ''

with open( 'host.conf', 'r' ) as f :
    HOST = f.read().replace( '\n', '' )

SAVED_FILE = './copay.ca'
CHECK_FILE = './check.txt'

SAVED_FILENOT = './noticias.ca'
CHECK_FILENOT = './checkNot.txt'
FTP_URLNOT = 'http://{}:8000/downloadNot'.format( HOST )

FTP_URL = 'http://{}:8000/download'.format( HOST )
HTTP_URL = 'http://{}:8080'.format( HOST )
player = scriptVideo.mediaplayer()


def traerNoticias (player):
    ca = ''
    contenido = False
    files = os.popen( ( 'DIR /B' if IS_WIN else 'ls -a' ) ).read().split( '\n' )
    
    for f in files :
        if f == '.contenidoNot' :
            contenido = True
        elif f == 'noticias.ca' :
            ca = f

    if not contenido :
        if ca == '' :
            show_img( 'notfound.png', 10 )
        else :
            if IS_WIN :
                os.system( 'unzip.exe -o {}'.format( ca ) )
            else :
                os.system( 'unzip -o {}'.format( ca ) )
            contenido = True   
            

    if contenido :
        f = io.open( '.contenidoNot/infoNot.ca', encoding="utf8")


        nuevaLin = ''
        for l in f :
            l = l.replace( '\n', '' )
            p = l.split( '&' )
            
            if (p[3] == 'no'):
                with io.open('.contenidoNot/descripcion.txt', encoding ="utf8") as f:
                    nuevo = " ".join(line.strip() for line in f) 
                    nuevito = nuevo.split('-*-')
                    otroMas = nuevito[int(p[4],10)].split('&')
                noticias.mostrar(player,p[0],otroMas[0],'no')           	
            	
            	
            	
            else :
                with io.open('.contenidoNot/descripcion.txt', encoding ="utf8") as f:
                    nuevo = " ".join(line.strip() for line in f) 
                    nuevito = nuevo.split('-*-')
                    otroMas = nuevito[int(p[4],10)].split('&')
                noticias.mostrar(player,p[0],otroMas[0],'.contenidoNot/{}'.format(p[3]))
            	


def reproducir (player) :
    ca = ''
    contenido = False
    files = os.popen( ( 'DIR /B' if IS_WIN else 'ls -a' ) ).read().split( '\n' )
    
    for f in files :
        if f == 'contenido' :
            contenido = True
        elif 'copay.ca' in f :
            ca = f
    
   
   
    
    if not contenido :
        if ca == '' :
            show_img( 'notfound.png', 10 )
        else :
            if IS_WIN :
                os.system( 'unzip.exe -o {}'.format( ca ) )
            else :
                os.system( 'unzip -o {}'.format( ca ) )
            contenido = True
    
    if contenido :
        f = open( 'contenido/info.ca', 'r' )
        
        for l in f :
            l = l.replace( '\n', '' )
            p = l.split( ',' )
            
            if p[2] == 'v' :
                player.showVideo( 'contenido/{}'.format( p[1] ) )
            else :
                
                player.showImage( 'contenido/{}'.format( p[1] ))
                time.sleep(int(p[3],10))
                player.teclaPresionada()

while True :

    if(player.is_init()):
        reproducir(player)
        if(player.is_init()):
            muestraClima.mostrar(player) 
            traerNoticias(player)
        else: sys.exit()
    else: sys.exit()


