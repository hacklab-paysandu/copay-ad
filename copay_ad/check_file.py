import io
import platform
import os
import socket
import requests


try:
    # For Python 3.0 and later
    from urllib.request import urlopen
    from urllib.error import URLError, HTTPError
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen



directorioAct = os.path.dirname(os.path.abspath(__file__))
IS_WIN = platform.system() is 'Windows'
HOST = ''

with open( '{}/host.conf'.format(directorioAct), 'r' ) as f :
    HOST = f.read().replace( '\n', '' )

SAVED_FILE = '{}\\copay.ca'.format(directorioAct)
CHECK_FILE = '{}/check.txt'.format(directorioAct)

SAVED_FILENOT = '{}/noticias.ca'.format(directorioAct)
CHECK_FILENOT = '{}/checkNot.txt'.format(directorioAct)
FTP_URLNOT = 'http://{}:8081/downloadNot'.format( HOST )

FTP_prueba = 'http://{}:8080/probandoId'.format( HOST )

FTP_URL = 'http://{}:8081/download'.format( HOST )
HTTP_URL = 'http://{}:8080'.format( HOST )


def download_file () :
    print( 'Downloading...' )
    print('{}\\contenido'.format( directorioAct) )
    if IS_WIN :

        os.system( 'DEL {}'.format( SAVED_FILE ) )
        os.system( 'RMDIR /S /Q {}\\contenido'.format(directorioAct) )

    else :
        os.system( 'rm {}'.format( SAVED_FILE ) )
        os.system( 'rm -r -f {}/contenido'.format(directorioAct) )
    #file = urllib2.urlopen( FTP_URL ).read()
    file = urlopen( FTP_URL ).read()
    
    with open( SAVED_FILE, 'wb' ) as f :
        f.write( file )

def check_file () :
    if not os.path.isfile( CHECK_FILE ) :
        with open( CHECK_FILE, 'w' ) as f :
            f.write( '-1' )
    
    #code = urllib2.urlopen( '{}/check'.format( HTTP_URL ) ).read().decode( 'utf-8' )
    try:
    	code = urlopen( '{}/check'.format( HTTP_URL ) ).read().decode( 'utf-8' )
    	actual = ''
    
    	with open( CHECK_FILE, 'r' ) as f :
        	actual = f.read()
    
    	if not actual == code :
        	download_file()
        
        	with open( CHECK_FILE, 'w' ) as f :
        		f.write( code )
    except URLError as e:
    	print("ERROR: Falló la conexión porque no está encendido el servidor, o porque en el archivo host.conf está mal puesta la IP del servidor")
    except HTTPError as e:
    	print(e.code)



def check_fileNot () :
    if not os.path.isfile( CHECK_FILENOT ) :
        with open( CHECK_FILENOT, 'w' ) as f :
            f.write( '-1' )
    #code = urllib2.urlopen( '{}/checkNot'.format( HTTP_URL ) ).read().decode( 'utf-8' )
    code = urlopen( '{}/checkNot'.format( HTTP_URL ) ).read().decode( 'utf-8' )
    actual = ''
    with open( CHECK_FILENOT, 'r' ) as f :
        actual = f.read()
    
    if not actual == code :
        download_fileNot()
        
        with open( CHECK_FILENOT, 'w' ) as f :
            f.write( code )


def download_fileNot () :
    print( 'Downloading...' )
    if IS_WIN :
        os.system( 'DEL {}'.format( SAVED_FILENOT ) )
        os.system( 'RMDIR /S /Q .contenidoNot' )
    else :
        os.system( 'rm {}'.format( SAVED_FILENOT ) )
        os.system( 'rm -r {}/.contenidoNot'.format(directorioAct) )
    #file = urllib2.urlopen( FTP_URLNOT ).read()
    file = urlopen( FTP_URLNOT ).read()
    
    with open( SAVED_FILENOT, 'wb' ) as f :
        f.write( file )




def probandoMonitoreo () :
	url = FTP_prueba
	print(url)
	payload = {'key1': 'value1', 'key2': 'value2'}
	r = requests.post(url, json=payload)
	print(r.text)




def internet(host="8.8.8.8", port=53, timeout=3):
	try:
		socket.setdefaulttimeout(timeout)
		socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
		return True
	except Exception as ex:
		#print ex.message
		return False

if(internet()):
   check_file()
   check_fileNot()
   #probandoMonitoreo();
