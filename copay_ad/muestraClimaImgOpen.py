#!/usr/bin/env python
# -*- coding: latin-1 -*-
import cv2
import numpy as np
from PIL import ImageFont, ImageDraw, Image
import requests
from xml.dom import minidom
import datetime
import locale
import scriptVideo
import time
import socket


TIEMPO = [
	['Intervalos nubosos', 'intervalos_nubosos.png'],
	['Cielos despejados', 'cielos_despejados.png'],
	['Tormenta', 'tormenta.png'],
	['Lluvia', 'lluvia.png'],
	['Intervalos nubosos con lluvias débiles','intervalos_nubosos_con_lluvias_debiles.png'],
	['Cielos nubosos','cielos_nubosos.png'],
	['Cielos nubosos con lluvias débiles','cielos_nubosos_con_lluvias_debiles.png'],
	['Cielos cubiertos', 'cielos_cubiertos.png'],
	['Cielos cubiertos con chubascos tormentosos','cielos_cubiertos_con_chubascos_tormentosos.png'],
	['Cielos cubiertos con lluvias débiles','cielos_cubiertos_con_lluvias_debiles.png']
]

#esto es codigo mio
locale.setlocale(locale.LC_TIME, '')


REMOTE_SERVER = "www.google.com"
def hay_conec(hostname):
  try:
    host = socket.gethostbyname(hostname)
    s = socket.create_connection((host, 80), 2)
    return True
  except:
     pass
  return False

if(hay_conec(REMOTE_SERVER)):
	re = requests.get('http://api.meteored.com.uy/index.php?api_lang=uy&localidad=13029&affiliate_id=53blsqeemd63&v=2.0&h=1')
	with open('tiempoHoras.xml','wb') as f:
		f.write(re.content)

tiempoHoy = minidom.parse('tiempoHoras.xml')



def text_wrap(text, font, max_width):
    lines = []
    # If the width of the text is smaller than image width
    # we don't need to split it, just add it to the lines array
    # and return
    if font.getsize(text)[0] <= max_width:
        lines.append(text) 
    else:
        # split the line by spaces to get words
        words = text.split(' ')  
        i = 0
        # append every word to a line while its width is shorter than image width
        while i < len(words):
            line = ''         
            while i < len(words) and font.getsize(line + words[i])[0] <= max_width:                
                line = line + words[i] + " "
                i += 1
            if not line:
                line = words[i]
                i += 1
            # when the line gets longer than the max width do not append the word, 
            # add the line to the lines array
            lines.append(line)    
    return lines




def temperaturaPorHora(hora, dia):
	if(hora==0):
		hora=24
	for hijo in dia.childNodes:
		if(hijo.tagName == "hour"):
			horita = (hijo.getAttribute('value'))
			nuevaHor = horita.replace(':00','')
			nuevaHoraInt = int(nuevaHor)
			if(nuevaHoraInt == hora):
				for hijito in hijo.childNodes:
					if(hijito.tagName == "temp"):
						return hijito.getAttribute('value')

def traerEstadoPorHora(hora, dia):
	if(hora==0):
		hora=24
	for hijo in dia.childNodes:
		if(hijo.tagName == "hour"):
			horita = (hijo.getAttribute('value'))
			nuevaHor = horita.replace(':00','')
			nuevaHoraInt = int(nuevaHor)
			if(nuevaHoraInt == hora):
				for hijito in hijo.childNodes:
					if(hijito.tagName == "symbol"):
						return hijito.getAttribute('desc')



def buscarEstadoEnLista(estado):
	for i in range(len(TIEMPO)):
		for j in range(len(TIEMPO[i])):
			if(TIEMPO[i][j] == estado):
				return i	
        		


now = datetime.datetime.now()
#horaHoy = now.hour
diaHoy =(now.strftime("%A")).capitalize()
diaNuevo = ""
#info del dia, como temperatura y eso
dias = tiempoHoy.getElementsByTagName('day')
for dia in dias:
	if dia.getAttribute('name') == diaHoy:
		diaNuevo = dia
		for hijo in dia.childNodes:
			if(hijo.tagName == "tempmin"):
				tempMin = hijo.getAttribute('value')
			if(hijo.tagName == "tempmax"):
				tempMax = hijo.getAttribute('value')
#hasta acaaaaaaaaaaaaaaaaa

#DISPLAY = 'img'


#cv2.namedWindow( DISPLAY, cv2.WND_PROP_FULLSCREEN )
#cv2.setWindowProperty( DISPLAY, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN )

def colocar_texto( img, texto, pos, tam = 15, col = ( 255, 255, 255, 255 ) ) :
	
	
	tam = int( tam )
	font = ImageFont.truetype( './fonts/fontClima.ttf', tam )
	#img_pil = Image.fromarray( img )
	img_pil = img
	draw = ImageDraw.Draw( img_pil )
	draw.text( pos, texto, font = font, fill = col )
	#return np.array( img_pil )
	return img_pil

def colocar_textoLindo( img, texto, pos, tam = 15, col = ( 255, 255, 255, 255 ) ) :
	
	tam = int( tam )
	font = ImageFont.truetype( './fonts/fontClima.ttf', tam )
	#img_pil = Image.fromarray( img )

	#image_size = img_pil.size
	image_size = img.size
	#draw = ImageDraw.Draw( img_pil )
	draw = ImageDraw.Draw( img )
	lineas = text_wrap(texto,font,image_size[0])
	line_height = font.getsize('hg')[1]
	x = 50
	y = 400
	for line in lineas:
    		draw.text((x, y), line, font = font , fill = col )
    		y = y + line_height

	#return np.array( img_pil )
	return img





def mostrar_clima ( duracion, estado, temperaturaActual, tempMinima, tempMaxima, player ) :
	#img = cv2.imread( './imagenes/plantilla.png' )
	img = Image.open( './imagenes/plantilla.png' )
	ANCHO,ALTO = img.size

	"""ANCHO = img.shape[1]
	ALTO = img.shape[0]"""
	#'{} °C'.format(temperaturaActual)
	img = colocar_texto( img, '{} C '.format(temperaturaActual), ( ANCHO * .3, ANCHO * .02 ), ALTO * .2 )
	img = colocar_textoLindo( img, TIEMPO[estado][0], ( ANCHO * .3, ANCHO * .02 + ALTO * .2 ), ALTO * .1 )
	img = colocar_texto( img, 'Temperatura minima: '+ '{} °C'.format(tempMinima), ( ANCHO * .05, ANCHO * .05 + ALTO * .7 ), ALTO * .1 )
	img = colocar_texto( img, 'Temperatura maxima: '+ '{} °C'.format(tempMaxima), ( ANCHO * .05, ANCHO * .05 + ALTO * .6 ), ALTO * .1 )

	img2 = cv2.imread( './imagenes/{}'.format(TIEMPO[estado][1]) )

	offx = int( ANCHO * .05 )
	offy = int( ANCHO * .02 )


	"""for x in range( img2.shape[1] ) :
		for y in range( img2.shape[0] ) :
			img[y + offy][x + offx] = img2[y][x]"""

	img.save('./imagenes/clima.png')
	#cv2.imwrite('./imagenes/clima.jpg',img)
	player.showImage( './imagenes/clima.png')
	time.sleep(10)
#esto de aca abajo tambien lo puse yo

def mostrar(player):
	fechaAct = datetime.datetime.now()
	hora = fechaAct.hour
	#cv2.namedWindow( displaycito, cv2.WND_PROP_FULLSCREEN )
	#cv2.setWindowProperty( displaycito, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN )
	posicionEstado = buscarEstadoEnLista(traerEstadoPorHora(hora,diaNuevo))
	mostrar_clima( 30, posicionEstado, temperaturaPorHora(hora,diaNuevo), tempMin, tempMax, player)
	#cv2.destroyAllWindows()

